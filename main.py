import cv2, lib.matcher as mt



def main():
    cap = cv2.VideoCapture(0)
    _, step_frame =  cap.read()
    while True:
        _, frame = cap.read()
        cv2.imshow('', frame)
        print(mt.detMatch(frame, step_frame))
        k = cv2.waitKey(1)
        if k == 27: # esc
            break
        step_frame = frame
    cap.release()
    cv2.destroyAllWindows()
    
if __name__ == '__main__':
    main()